module.exports = {
    env: {
      // We're running in a node.js environment
      node: true,
      // We're using the Jest testing library, and its global functions
      //'jest/globals': true
      'mocha/globals': true
    },
    extends: [
      // We'll begin with the standard set of rules, and tweak from there
      'eslint:recommended'
    ],
    plugins: [
      // Use the jest eslint plugin to help catch any test-specific lint issues
      //'jest'
      'mocha'
    ],
    rules: {
      // Define a few custom eslint rules
      'indent': ['error', 2],
      'linebreak-style': ['error', 'unix'],
      'quotes': ['error', 'single'],
      'semi': ['error', 'always'],
      'eqeqeq': ['error', 'always'],
      // Some rules can be warnings vs. errors
      'no-console': ['warn']
    }
  };