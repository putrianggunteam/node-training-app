const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');
 
const {app} = require('./../server');
const {Course} = require('./../models/course');



const course = [{
    _id: new ObjectID(),
    courseName: 'Low Intermediate 1'
},{
    _id: new ObjectID(),
    courseName: 'Low Intermediate 2',
    trainer: 'Uncle',
    createdBy: 'Putri',
    passingGrade: 75,
    maxAbsent: 3
}];

beforeEach((done) => {
    Course.remove({}).then(() => {
        return Course.insertMany(course);
    }).then(() => done());
});

describe ('POST /course', () => {
    it('should create a new course', (done) => {
        var courseName = 'Intermediate';

        request(app)
        .post('/course')
        .send({courseName})
        .expect(200)
        .expect((res) => {
            expect(res.body.course.courseName).toBe(courseName);
        })
        .end((err, res) => {
            if(err) {
                return done(err);
            }

            Course.find({courseName}).then((course) => {
                expect(course.length).toBe(1);
                expect(course[0].courseName).toBe(courseName);
                done();
            }).catch((e) => done(e));
        });
    });

    it('should not create todo with invalid body data', (done) => {
        request(app)
        .post('/course')
        .send({})
        .expect(400)
        .end((err, res) => {
            if (err) {
                return done(err);
            }
            Course.find().then((courses) => {
                expect(courses.length).toBe(2);
                done();
            }).catch((e) => done(e));
        });
    });
});

describe('GET /course/:id', () => {
    it('should return course detail', (done) => {
        request(app)
        .get(`/course/${course[0]._id.toHexString()}`)
        .expect(200)
        .expect((res) => {
            expect(res.body.course.courseName).toBe(course[0].courseName);
        })
        .end(done);
    });

    it('should return 404 if todo not found', (done) => {
        var hexId = new ObjectID().toHexString();

        request(app)
        .get(`/course/${hexId}`)
        .expect(404)
        .end(done);
    });

    it('should return 404 for non-object ids', (done) => {
        request(app)
        .get('/course/123abc')
        .expect(404)
        .end(done);
    });
    
});