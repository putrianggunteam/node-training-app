var mongoose = require ('mongoose');

var Schedule = mongoose.model('Schedule', {
    course: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    trainer: {
        type: String,
        default : null
    }, 
    backupTrainer: {
        type: String,
        default: null
    },
    classRoom : {
        type: String,
        default : null
    },
    scheduleType : {
        type: String,
        default : null
    },
    day : {
        type: String,
        default : null
    },
    startTime: {
        type: Number,
        default: null
    },
    endTime: {
        type: Number,
        default: null
    },
    capacity: {
        type: Number,
        default: null
    },
    participant: {
        type: Object,
        default: null
    }
});

module.exports = {Schedule};