var mongoose = require ('mongoose');

var ClassParticipant = mongoose.model('ClassParticipant', {
    training: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    date : {
        type: Date,
        default : null
    },
    activity: {
        type: String,
        required: true
    },
    note: {
        type: String,
        required: true
    },
    attend: {
        type: Boolean,
        default:false
    }
});

module.exports = {ClassParticipant};
