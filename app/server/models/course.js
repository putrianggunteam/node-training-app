var mongoose = require('mongoose');

var Course = mongoose.model('Course', {
    courseName: {
        type: String,
        required: true,
        minlength: 3,
        trim: true
    },
    trainer: {
        type: String,
        default: null
    },
    createdBy: {
        type: String,
        default: null        
    },
    updateBy: {
        type: Date,
        default: null
    }, 
    passingGrade: {
        type: Number,
        default: null
    },
    maxAbsent: {
        type: Number,
        default: null
    }, 
    assesmentDetails: {
        type: String,
        default: null
    }
});

module.exports = {Course}; 