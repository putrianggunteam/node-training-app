var mongoose = require ('mongoose');

var ClassAssementResult = mongoose.model('ClassAssementResult', {
    training: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    employeeID: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    midTest: {
        type: Number,
        default : null
    },
    finalTest: {
        type: Number,
        default : null
    },
    average: {
        type: Number,
        default : null
    },
    workshop: {
        type: String,
        default : null
    },
    comment: {
        type: String,
        default : null
    },
    status: {
        type: String,
        default : null
    },
});

module.exports = {ClassAssementResult};