var mongoose = require('mongoose');

var Period = mongoose.model('Period', {
    periodName: {
        type: String,
        required: true,
        minlength: 3,
        trim: true
    }, 
    active: {
        type: Boolean,
        default: false
    },
    courses: {
        type: Number,
        default: null
    },
    startDate: {
        type: Date,
        default : null
    },
    endDate: {
        type: Date,
        default : null
    },
    createdBY: {
        type: String,
        default: null        
    },
    updateBy: {
        type: Date,
        default: null
    }
});

module.exports = {Period}; 
