var mongoose = require('mongoose');

var Enrolment = mongoose.model('Enrolment', {
    course: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    period: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    startDate: {
        type: Date,
        default : null
    },
    endDate: {
        type: Date,
        default : null
    },
    trainer: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    status: {
        type: Boolean,
        default: null
    }
});

module.exports = {Enrolment};