var mongoose = require('mongoose');

var Training = mongoose.model('Training', {
    course: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    period: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    startDate: {
        type: Date,
        default : null
    },
    endDate: {
        type: Date,
        default : null
    },
    room: {
        type: String,
        default: null
    }
});

module.exports = {Training}; 