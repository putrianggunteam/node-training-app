var mongoose = require('mongoose');

var Achievement = mongoose.model('Achievement', {
    employeeID : {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        minlength: 3,
        trim: true
    },
    achievement: {
        type: Object,
        default: null
    }

});

module.exports = {Achievement};