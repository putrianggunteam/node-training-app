const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');

var {mongoose} = require('./db/mongoose');
var {Course} = require('./models/course');
var {Training} = require('./models/training');
var {Period} = require('./models/period');

var app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.post('/course', (req, res) => {
    var course = new Course({
        courseName: req.body.courseName,
        trainer: req.body.trainer,
        createdBy: req.body.createdBy,
        updateBy: req.body.updateBy,
        passingGrade: req.body.passingGrade,
        minAbsent: req.body.minAbsent,
        scoreCalculation: req.body.scoreCalculation        
    });

    course.save().then((doc) => {
        res.send({course});
        }, (e) => {
            res.status(400).send(e);
        });
    });

app.get('/course', (req, res) => {
    Course.find().then((course) => {
        res.send({course});
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/course/:id', (req, res) => {
    var id = req.params.id;

    if(!ObjectID.isValid(id)){
        return res.status(404).send();
    }

    Course.findById(id).then((course) => {
        if(!course) {
            return res.status(404).send();
        }

        res.send({course});
    }).catch((e) => {
        res.status(400).send();
    });
});

app.delete('/course/:id', (req, res) => {
    var id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Course.findByIdAndDelete(id).then((course) => {
        if(!course) {
            return res.status(404).send();
        }

        res.send({course});
    }).catch((e) => {
        res.status(400).send();
    });
});

app.patch('/course/:id', (req, res) => {
    var id = req.params.id;
    var body = _.pick(req.body, ['courseName', 'trainer', 'createdBy', 'updateBy', 'passingGrade', 'maxAbsent', 'assesmentDetails']);

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Course.findByIdAndUpdate(id, {$set: body}, {new: true}).then((course) => {
        if (!course) {
            return res.status(404).send();
        }

        res.send({course});
    }). catch((e) => {
        res.status(400).send();
    })
});

app.listen(port, () =>{
    console.log(`Started up at port ${port}`);
});

module.exports = {app};
