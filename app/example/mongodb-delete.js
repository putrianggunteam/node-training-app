const {MongoClient, ObjectID} = require('mongodb');
var obj = new ObjectID();

MongoClient.connect('mongodb://localhost:27017/TrainingApp', (err, client) => {
    if(err) {
       return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    const db = client.db('TrainingApp')

    //------deleteMany-----    
    // db.collection('Users').deleteMany({name: 'Putri'}).then((result) => {
    //     console.log(result);
    // });

    //-------deleteOne--------
    // db.collection('Users').deleteOne({name: 'Anggun'}).then((result) => {
    //     console.log(result);
    // });

    //findOneAndDelete
    db.collection('Users').findOneAndDelete({name: 'Anggun'}).then((result) => {
        console.log(result);
    });

    //client.close();
});