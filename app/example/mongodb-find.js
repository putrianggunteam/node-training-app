const {MongoClient, ObjectID} = require('mongodb');
var obj = new ObjectID();

MongoClient.connect('mongodb://localhost:27017/TrainingApp', (err, client) => {
    if(err) {
       return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    const db = client.db('TrainingApp')

    db.collection('Users').find({
        name: 'Anggun'
    }).toArray().then((docs) => {
        console.log('Users');
        console.log(JSON.stringify(docs, undefined, 2));
    }, (err) => {
        console.log('Unable to fetch data', err);
    })

    // db.collection('Users').find({
    //     grade: 8,
    //     _id: new ObjectID('5bdfd8bd9fc9bbca68cfa38a')
    // }).toArray().then((docs) => {
    //     console.log('Users');
    //     console.log(JSON.stringify(docs, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch data', err);
    // })

    // db.collection('Users').find().count().then((count) => {
    //     console.log(`Users count: ${count}`);
    // }, (err) => {
    //     console.log('Unable to fetch data', err);
    // })

    client.close();
});