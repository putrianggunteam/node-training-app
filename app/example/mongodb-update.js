const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TrainingApp', (err, client) => {
    if(err) {
       return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    const db = client.db('TrainingApp')

    // db.collection('Users').findOneAndUpdate({
    //     _id: new ObjectID('5bdfb9012485d3bf98efe0ca')
    // }, {
    //     $set: {
    //         name: 'Putri Anggun',
    //         jobFamily: 'SE',
    //         grade: 11,
    //         officeLocation: 'Bali'

    //     }
    // }, {
    //     returnOriginal: false
    // }). then((result) => {
    //     console.log(result);
    // });

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID('5bdfb9012485d3bf98efe0ca')
    }, {
        $set: {
            name: 'Putri Anggun Rohmalia',
        },
        $inc: {
            grade: 1
        }
    }, {
        returnOriginal: false
    }). then((result) => {
        console.log(result);
    });


    //client.close();
});